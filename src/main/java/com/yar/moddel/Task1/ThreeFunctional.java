package com.yar.moddel.Task1;

@FunctionalInterface
public interface ThreeFunctional {
    int calculate(int a, int b, int c);
}
