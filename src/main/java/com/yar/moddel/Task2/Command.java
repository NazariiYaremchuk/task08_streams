package com.yar.moddel.Task2;

@FunctionalInterface
public interface Command {
    String getString(String str);
}
