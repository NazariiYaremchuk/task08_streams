package com.yar.moddel.Task3;

import java.util.*;
import java.util.stream.Stream;

public class Task3 {
    /**
     * Create a few methods that returns list (or array) of random integers. Methods should use streams API and
     * should be implemented using different Streams generators.
     * Count average, min, max, sum of list values. Try to count sum using both reduce and sum Stream methods
     * Count number of values that are bigger than average
     */
    List<Integer> integerList = new ArrayList<>();

    private void initList() {
        for (int i = 0; i < 20; i++) {
            integerList.add(new Integer((int) (Math.random() * 100)));
        }
    }

    public void toStream() {
        Stream<Integer> stream = Stream.generate(() -> new Integer((int) (Math.random() * 100))).limit(20);
        Stream<Integer> stream1 = Stream.iterate(2, t -> (t + 5));
        Stream<Integer> stream2 = Stream.of(5, 6, 3, 9);
    }

    public Map<String,Integer> getStreamInfo(Stream<Integer> stream) {
        Map<String, Integer> map = new HashMap<>();
        map.put("min", stream.min(Integer::compareTo).get());
        map.put("max", stream.max(Integer::compareTo).get());

        map.put("average", (calculateSumm(stream)/ (int)(stream.count())));
        map.put("sum", new Integer(calculateSumm(stream)));
        map.put("large", (int)stream.filter((s)->(s>map.get("average"))).count());
        return map;
    }


    private Integer calculateSumm(Stream<Integer> stream){
        Integer summ = stream.reduce((sum, elem) -> (sum += elem)).get();
        return summ;
    }

}
